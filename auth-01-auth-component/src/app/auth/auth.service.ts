import {HttpClient,HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError,tap} from 'rxjs/operators';
import {throwError,pipe, BehaviorSubject} from 'rxjs';

import {User} from './user.model';
import {Router} from '@angular/router';

export interface AuthResponseData {
    kind: string;
    idToken: string;
    email: string;
    refreshToken: string;
    expiresIn: string;
    localId: string;
    registered?: boolean;
}


@Injectable({providedIn: 'root'})
export class AuthService {

    user = new BehaviorSubject<User>(null); // mit .next auf das Subject kann ich immer wieder einen
                                        // neuen User emitten. 
                                        
    private tokenExpirationTimer: any;


    constructor(private http: HttpClient, private router: Router) {}

    logout() {
        this.user.next(null);
        this.router.navigate(['/auth']);
        localStorage.removeItem('userData');
        if (this.tokenExpirationTimer){
            clearTimeout(this.tokenExpirationTimer)
        }
        this.tokenExpirationTimer = null;
    }

    autoLogout(expirationDuration: number) {
        console.log(expirationDuration)
        this.tokenExpirationTimer =  setTimeout(() => {
               this.logout();
           }, expirationDuration)
    }

    singUp(email: string, password: string) {
        return this.http.post<AuthResponseData>(
            'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAxtRvvkWEwnhEKQ9fGvdlOdbXeD7d7Q9Y',
            {
                email: email,
                password: password,
                returnSecureToken: true
            }
        ).pipe(catchError(this.handleError), tap(resData => {
            this.handleAuthentication(
                resData.email,
                resData.localId,
                resData.idToken,
                +resData.expiresIn)
        }));
    }

    login(email: string,password: string) {
        return this.http.post<AuthResponseData>(
            'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAxtRvvkWEwnhEKQ9fGvdlOdbXeD7d7Q9Y',
            {
                email: email,
                password: password,
                returnSecureToken: true
            }
        ).pipe(catchError(this.handleError), tap(resData => {
            this.handleAuthentication(
                resData.email,
                resData.localId,
                resData.idToken,
                +resData.expiresIn)
        }));
    }

    autoLogin() {
        const userData: {
            email: string;
            id: string;
            _token: string;
            _tokenExpirationDate: string;
        } = JSON.parse(localStorage.getItem('userData'));
        if (!userData) {
            return;
        }

        const loadedUser = new User(
            userData.email,
            userData.id,
            userData._token,
            new Date(userData._tokenExpirationDate)
        );
        if (loadedUser.token) {
            this.user.next(loadedUser);
            const expirationDuration = 
            new Date(userData._tokenExpirationDate).getTime() - 
            new Date().getTime();
            this.autoLogout(expirationDuration)
        }
    }

    private handleAuthentication(email: string,userId: string, _token: string,expiresIn: number) {
        const expirationDate = new Date(new Date().getTime() + (expiresIn * 1000));
        const user = new User(
            email,
            userId,
            _token,
            expirationDate);
        this.user.next(user);
        this.autoLogout(expiresIn * 1000)
        localStorage.setItem('userData', JSON.stringify(user))

    }

    private handleError(errorRes: HttpErrorResponse) {
        let errorMessage = 'An unknown Error occured';
        if (!errorRes.error || !errorRes.error.error) {
            return throwError(errorMessage)
        }
        switch(errorRes.error.error.message) {
            case 'EMAIL_EXISTS':
                errorMessage = 'Email already exists. Please choose another one.';
                break;
            case 'EMAIL_NOT_FOUND':
                errorMessage = 'Email does not exist.'
                break;
            case 'INVALID_PASSWORD':
                errorMessage = 'Password is not valid'
                break;
        }
        return throwError(errorMessage);
    }
}
