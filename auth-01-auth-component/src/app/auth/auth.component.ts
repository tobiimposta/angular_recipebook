import {Component, ComponentFactoryResolver, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService, AuthResponseData} from './auth.service';
import {Observable, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {AlertComponent} from '../shared/alert/alert.component';
import {PlaceholderDirective} from '../shared/placeholder/placeholder.directive'
import {EventEmitter} from 'protractor';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html'
})
export class AuthComponent {

  constructor(
    private authService: AuthService, 
    private router: Router, 
    private cFr: ComponentFactoryResolver) {}

  isLoginMode = true;
  isLoading = false;
  error: string = null;
  errorOccured = false;
  @ViewChild(PlaceholderDirective, {static: false}) alertHost: PlaceholderDirective;
 
    private closeSub: Subscription;


  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  onSubmit(authForm: NgForm) {
    if (!authForm.valid) {
      return;
    }
    const email = authForm.value.email;
    const password = authForm.value.password;

    let authObs: Observable<AuthResponseData>;

    this.isLoading = true;
    if (this.isLoginMode) {
      authObs = this.authService.login(email, password)

    } else {
      authObs = this.authService.singUp(email, password)
    }

    authObs.subscribe(resData => {
      console.log(resData)
      this.isLoading = false;
      this.router.navigate(['/recipes']);
    }, errorMessage => {
      this.errorOccured = true;
      console.log(errorMessage)
      this.error = errorMessage;
      this.showErrorAlert(errorMessage);
      this.isLoading = false;
      authForm.reset();
    })
  }
  onHandleError(){
    this.error = null;
  }

  private showErrorAlert(message: string){
      const alertCmpFactory = this.cFr.resolveComponentFactory(
        AlertComponent
        );
      const hostViewContainerRef = this.alertHost.viewContainerRef
      hostViewContainerRef.clear();

      const componentRef = hostViewContainerRef.createComponent(alertCmpFactory);
        
      componentRef.instance.message = message;
     this.closeSub = componentRef.instance.close.subscribe(()=> {
          this.closeSub.unsubscribe();
          hostViewContainerRef.clear();
      }) 
   
    }

    
}
